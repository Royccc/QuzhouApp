import accountApi from '../../api/account'
import appApi from '../../api/app'

// initial state
const state = {
  serNum: ''
}

// getters
const getters = {
  serNum: state => state.serNum
}

// actions
const actions = {
  // 获取随机数
  getRandomNum ({ dispatch, commit, getters, rootGetters }, params) {
    return new Promise((resolve, reject) => {
      appApi.getRandomNum({...params, userId: getters.userInfo.userId}).then(res => {
        console.log(res)
        resolve(res.data.randomNum)
      }).catch(err => reject(err))
    })
  },
  // 设置密码
  setPassword ({ dispatch, commit, getters, rootGetters }, params) {
    return new Promise((resolve, reject) => {
      accountApi.setPassword({...params, userId: getters.userInfo.userId}).then(res => {
        resolve(res)
      }).catch(err => reject(err))
    })
  },
  // 修改密码
  updatePassword ({ dispatch, commit, getters, rootGetters }, params) {
    return new Promise((resolve, reject) => {
      accountApi.updatePassword({...params, userId: getters.userInfo.userId}).then(res => {
        resolve(res)
      }).catch(err => reject(err))
    })
  },
  // 找回密码点击下一步（重置密码）
  resetPassword ({ dispatch, commit, getters, rootGetters }, params) {
    return new Promise((resolve, reject) => {
      accountApi.resetPassword({...params, userId: getters.userInfo.userId}).then(res => {
        resolve(res)
      }).catch(err => reject(err))
    })
  },
  // 找回密码提交（重置密码）
  resetPasswordConfirm ({ dispatch, commit, getters, rootGetters }, params) {
    return new Promise((resolve, reject) => {
      accountApi.resetPasswordConfirm({...params, userId: getters.userInfo.userId}).then(res => {
        resolve(res)
      }).catch(err => reject(err))
    })
  },
  // 关闭实人认证提交
  closeMobilePayment ({ dispatch, commit, getters, rootGetters }, params) {
    return new Promise((resolve, reject) => {
      accountApi.closeMobilePayment({...params, userId: getters.userInfo.userId}).then(res => {
        resolve(res)
      }).catch(err => reject(err))
    })
  },
  // 获取验证码
  getVerifyCode ({ dispatch, commit, getters, rootGetters }, params) {
    return new Promise((resolve, reject) => {
      accountApi.getVerCode({...params, userId: getters.userInfo.userId}).then(res => {
        commit('setSerNum', res.data)
        resolve(res)
      }).catch(err => reject(err))
    })
  }
}

// mutations
const mutations = {
  setSerNum (state, data) {
    state.serNum = data.serNum
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
