import createApi from './createApi'
import { baseConf } from './index'

// 账号设置页面
export default {
  // 设置密码
  setPassword: data => createApi(baseConf, {url: '/user/setPaymentPasswordwithVerCode', data}),
  // 修改密码
  updatePassword: data => createApi(baseConf, {url: '/user/updateMedicarePaymentPassword', data}),

  // 找回密码点击下一步
  resetPassword: data => createApi(baseConf, {url: '/user/getBackMedicarePaymentPwdSubmit', data}),
  // 找回（重置）密码
  resetPasswordConfirm: data => createApi(baseConf, {url: '/user/setMedicarePaymentPasswordSubmit', data}),

  // 关闭实人认证提交
  closeMobilePayment: data => createApi(baseConf, {url: '/user/closeCertificationSubmit', data}),
  // 获取手机验证码
  getVerCode: data => createApi(baseConf, {url: '/user/getVerCode', data})
}
